extends SubViewport

var flip_enabled := true
var keep_flipping := false


func _ready():
	$Spin.play("Spin")
	$Rock.play("Rock")


func pop():
	$Pop.play("pop")


func flip():
	if flip_enabled:
		$Flip.play("Flip")


func spin(active: bool):
	if active:
		$Spin.play("Spin")
	else:
		$Spin.play("RESET")


func rock(active: bool):
	if active:
		$Rock.play("Rock")
	else:
		$Rock.play("RESET")


func set_flip(active: bool):
	flip_enabled = active


func set_flipping(active: bool):
	keep_flipping = active
	if keep_flipping:
		flip()


func set_color(color: Color):
	$Donut/Gimbal/Icing.get_active_material(0).albedo_color = color


func _on_flip_animation_finished(_anim_name):
	if keep_flipping:
		flip()
