extends Control

@export var nausea_curve: Curve
@onready var score_label = $HBoxContainer/CenterPanel/VBoxContainer/MarginContainer/VBoxContainer/Label
@onready var donut_viewport = $HBoxContainer/CenterPanel/VBoxContainer/Container/Button/TabContainer/SubViewportContainer/Donut_Viewport
@onready var donut_tabs = $HBoxContainer/CenterPanel/VBoxContainer/Container/Button/TabContainer
@onready var slider = $HBoxContainer/MenuPanel/MarginContainer/TabContainer/Cheats/HSlider
@onready var tab_container = $HBoxContainer/MenuPanel/MarginContainer/TabContainer
@onready var clickers := [
	$HBoxContainer/UpgradesPanel/MarginContainer/VSplitContainer/ScrollContainer2/VBoxContainer/Clicker,
	$HBoxContainer/UpgradesPanel/MarginContainer/VSplitContainer/ScrollContainer2/VBoxContainer/Baker,
	$HBoxContainer/UpgradesPanel/MarginContainer/VSplitContainer/ScrollContainer2/VBoxContainer/Farm,
	$HBoxContainer/UpgradesPanel/MarginContainer/VSplitContainer/ScrollContainer2/VBoxContainer/Mine,
	$HBoxContainer/UpgradesPanel/MarginContainer/VSplitContainer/ScrollContainer2/VBoxContainer/Factory,
	$HBoxContainer/UpgradesPanel/MarginContainer/VSplitContainer/ScrollContainer2/VBoxContainer/Bank,
	$HBoxContainer/UpgradesPanel/MarginContainer/VSplitContainer/ScrollContainer2/VBoxContainer/Shipment,
	$HBoxContainer/UpgradesPanel/MarginContainer/VSplitContainer/ScrollContainer2/VBoxContainer/Portal,
	$"HBoxContainer/UpgradesPanel/MarginContainer/VSplitContainer/ScrollContainer2/VBoxContainer/Donut Man"
]
@onready var squiggles = $HBoxContainer/CenterPanel/VBoxContainer/Container/MarginContainer2/SubViewportContainer/SubViewport/NauseaSquiggles
@onready var squiggles_2 = $HBoxContainer/CenterPanel/VBoxContainer/Container/MarginContainer2/SubViewportContainer/SubViewport/NauseaSquiggles2
@onready var background = $HBoxContainer/CenterPanel/MarginContainer/Background

var click_modifier := 1
var click_power := 1
var nausea_squiggle_rotation_speed := 1.0
var nause_squiggle_curve_time := 3.0
var nausea_squiggle_time := 0.0
var horrible_rotations := false
var pop := true


func _ready():
	tab_container.set_tab_hidden(1, true)
	background.play("Slide")

func _process(delta):
	if horrible_rotations:
		nausea_squiggle_time += delta
		if nausea_squiggle_time > nause_squiggle_curve_time:
			nausea_squiggle_time = 0
		squiggles.rotate(nausea_squiggle_rotation_speed * delta * nausea_curve.sample_baked(nausea_squiggle_time / nause_squiggle_curve_time))
		squiggles_2.rotate(-nausea_squiggle_rotation_speed * delta * nausea_curve.sample_baked(nausea_squiggle_time / nause_squiggle_curve_time))
	else:
		squiggles.rotate(nausea_squiggle_rotation_speed * delta)
		squiggles_2.rotate(-nausea_squiggle_rotation_speed * delta)


func _on_button_pressed():
	_add_score(click_power * click_modifier)
	if pop:
		donut_viewport.pop()


func _on_upgrade_pressed(item: int, color: Color, cost: int):
	_add_score(-cost)
	if item >= 0:
		clickers[item].upgrade(color)
	else:
		click_power *= 2


func _on_clicker_auto_click(value):
	_add_score(value)


func _add_score(value):
	Score.score += value
	score_label.text = "%s Donuts" % Score.score
	slider.set_value_no_signal(Score.score)
	slider.max_value = max(Score.score ** 2, 100)


func _on_clicker_bought(cost):
	_add_score(-cost)
	donut_viewport.flip()


func _on_spin_box_value_changed(value):
	click_modifier = value


func _on_h_slider_drag_ended(value_changed):
	if value_changed:
		_add_score(0) # Refresh everything.


func _on_h_slider_value_changed(value):
	Score.score = value
	score_label.text = "%s Donuts" % Score.score


func _on_text_edit_text_changed():
	var t = $HBoxContainer/MenuPanel/MarginContainer/TabContainer/Diary/TextEdit.text
	if t.to_lower() == "cheatz":
		tab_container.set_tab_hidden(1, false)


func _on_icing_color_color_changed(color):
	donut_viewport.set_color(color)


func _on_spinning_toggled(button_pressed):
	donut_viewport.spin(button_pressed)


func _on_rocking_toggled(button_pressed):
	donut_viewport.rock(button_pressed)


func _on_flipping_toggled(button_pressed):
	donut_viewport.set_flip(button_pressed)


func _on_more_flipping_toggled(button_pressed):
	donut_viewport.set_flipping(button_pressed)


func _on_viewport_tab_item_selected(index):
	donut_tabs.current_tab = index


func _on_squiggles_toggled(button_pressed):
	squiggles.visible = button_pressed
	squiggles_2.visible = button_pressed


func _on_squiggle_speed_value_changed(value):
	nausea_squiggle_rotation_speed = value


func _on_horrible_rotations_toggled(button_pressed):
	horrible_rotations = button_pressed


func _on_invert_time_value_changed(value):
	nause_squiggle_curve_time = value


func _on_pop_toggled(button_pressed):
	pop = button_pressed


func _on_background_color_color_changed(color):
	$HBoxContainer/CenterPanel/MarginContainer/TextureRect.modulate = color


func _on_background_move_item_selected(index):
	match index:
		0:
			background.play("RESET")
		1:
			background.play("Slide")
		2:
			background.play("Wobble")


func _on_background_speed_value_changed(value):
	background.speed_scale = value
