extends Button

@export var item_num := 0
@export var clicker_name := "None"
@export var my_color := Color.WHITE
@export var base_cost := 100.0
@export var cost_level := 1
var my_cost: float

signal upgrade_pressed(item, color, cost)


func _ready():
	my_cost = base_cost ** cost_level
	tooltip_text = "Double the production of %s.\nCosts %s donuts." % [clicker_name, my_cost]
	$ColorRect.set_color(my_color)


func _process(_delta):
	visible = my_cost / 2 < Score.score
	disabled = my_cost > Score.score
	$ColorRect2.visible = !disabled

func _on_pressed():
	emit_signal("upgrade_pressed", item_num, my_color, my_cost)
	queue_free()
