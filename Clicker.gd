extends Button

@export var wait_time := 5.0
@export var base_value := 1
@export var multiplier := 1
@export var cost := 15
@export var num_owned := 0
@onready var progress = $AspectRatioContainer/MarginContainer/Progress
@onready var progress_bar = $ProgressBar
@onready var color_rect = $AspectRatioContainer/MarginContainer/ColorRect
@onready var color_rect_2 = $AspectRatioContainer/MarginContainer/ColorRect2
@onready var timer = $Timer
var real_wait_time: float
var level := 1

signal clicker_bought(my_cost)
signal auto_click(value)


func _ready():
	text = "%s (%s)" % [name, cost]
	$AspectRatioContainer/MarginContainer.visible = true
	progress.texture_under = icon
	progress.texture_progress = icon
	# Make 5 seconds actually feel like 5 seconds by making it less than 5 seconds.
	real_wait_time = wait_time / 1.5

func _process(_delta):
	disabled = Score.score < cost
	color_rect_2.visible = disabled
	progress.set_value_no_signal((Score.score / cost) * 100.0)
	if !timer.is_stopped():
		progress_bar.set_value_no_signal(((timer.wait_time - timer.time_left) / real_wait_time) * 100.0)


func upgrade(color: Color):
	multiplier *= 2
	level += 1
	_update_tooltip()
	color_rect.set_color(color)


func _on_timer_timeout():
	emit_signal("auto_click", base_value * multiplier * num_owned)


func _on_pressed():
	emit_signal("clicker_bought", cost)
	cost = ceil(cost * 1.13)
	num_owned += 1
	timer.start(real_wait_time)
	_update_tooltip()
	text = "%s (%s)" % [name, cost]


func _update_tooltip():
	if num_owned == 1:
		tooltip_text = "%s level %s %s owned producing\n%s donuts every %s sec\nfor a total of %s donuts/sec" % \
		[num_owned, level, name, base_value * multiplier, wait_time, (base_value * multiplier * num_owned) / wait_time]
	else:
		tooltip_text = "%s level %s %ss owned\neach producing %s donuts every %s sec\nfor a total of %s donuts/sec" % \
		[num_owned, level, name, base_value * multiplier, wait_time, (base_value * multiplier * num_owned) / wait_time]
