# Welcome to the amazing Nausea Donut Game!

This repo is a companion for the [Every Control Node](https://youtu.be/sb6xe87RUE4) video I made.

If you are looking for a specific control node, check the [Commit history](https://gitlab.com/lazy-tutorials/every-control-node/-/commits/main?ref_type=heads).
For example, I used the Reference Rect in commit number d46c2ec3. You can use the following commands to view this particular commit:

    git clone https://gitlab.com/lazy-tutorials/every-control-node.git Tutorial
    cd Tutorial

    git checkout d46c2ec3

At this point, the showroom will contain a Reference Rect node, and the game will match the video at the 45 minute mark where this node was introduced.

You can return to the latest version of the game using the command `git checkout main`